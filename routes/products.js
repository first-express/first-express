const express = require('express')
const router = express.Router()

const Product = require('../models/Product')

const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        msg: 'Product not found'
      })
    }
    res.json(product)
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

const addProducts = async function (req, res, next) {
  // console.log(req.body)
  // const newProduct = {
  //   id: lastId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // products.push(newProduct)
  // lastId++
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }

  // const product = {
  //   id: productId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products[index] = product
  //   res.json(products[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Product id ' + req.params.id
  //   })
  // }
}

const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(200).send()
  } catch (error) {
    return res.status(404).send({ message: error.message })
  }
  // const index = products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   products.splice(index, 1)
  //   res.status(200).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No Product id ' + req.params.id
  //   })
  // }
}

router.get('/', getProducts) // GET All Products
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // POST (Add) Product
router.put('/:id', updateProduct)
router.delete('/:id', deleteProduct)

module.exports = router
