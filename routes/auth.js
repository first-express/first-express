const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const User = require('../models/User')

const { generateAccessToken } = require('../helpers/auth')
const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password
  try {
    const user = await User.findOne({ username: username }).exec()
    const verifyResult = await bcrypt.compare(password, user.password)
    if (!verifyResult) {
      return res.status(404).json({
        msg: 'User not found'
      })
    }
    const token = generateAccessToken({ _id: user._id, username: user.username })

    res.json({ user: { _id: user._id, name: user.username, roles: user.roles }, token: token })
  } catch (error) {
    return res.status(404).json({
      msg: error.message
    })
  }
}

router.post('/login', login)

module.exports = router
